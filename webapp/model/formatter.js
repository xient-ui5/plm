sap.ui.define([ "sap/ui/core/ValueState" ], function(ValueState) {
	"use strict";
	return {
		numberUnit : function(sValue) {

		},
		/**
		 * Defines a value state based on the stock level
		 * 
		 * @public
		 * @param {number}
		 *            iValue the stock level of a product
		 * @returns {string} sValue the state for the stock level
		 */
		quantityState : function(iValue) {
			if (iValue === 0) {
				return ValueState.Error;
			} else if (iValue <= 10) {
				return ValueState.Warning;
			} else {
				return ValueState.Success;
			}
		}
	};

});