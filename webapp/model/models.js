sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function(JSONModel, Device) {
	"use strict";

	return {

		createDeviceModel: function() {
			/**var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel; */		
			
			var sServiceUrl = "/sap/opu/odata/SAP/ZPLM_SRV/";
			//Adding service to the odata model
			var oModel = new sap.ui.model.odata.ODataModel(sServiceUrl, false);
	
			sap.ui.getCore().setModel(oModel);
			
		}

	};
});