sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast"
], function(Controller) {
	"use strict";

	return Controller.extend("PLM.controller.PhaseOut", {
		/**
		 * Navigates to the worklist when the link is pressed
		 * 
		 * @public
		 */
		onInit: function() {
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.file = null;
		},

		onCancel: function() {
			var clear = document.getElementsByTagName("Input");
			for (var i = 0; i < clear.length; i++) {
				if (clear[i].type === "text") {
					clear[i].value = "";
				}
			}
		},

		onNavBack: function() {
			this.oRouter.navTo("overview");

			/*
			var oHistory = sap.ui.core.routing.History.getInstance(),
				sPreviousHash = oHistory.getPreviousHash();
			if (sPreviousHash !== undefined) {
				// The history contains a previous entry
				history.go(-1);
			}
			*/
		},

		onUploadFile: function() {
			/*
			 *
			 * Collect data of the project and send collected data to back-end.
			 * If a file was selected, the upload of the file is the second step.
			 *
			 */

			var data = {};
			var oView = this.getView();
			data.ProjectName = oView.byId("proj_name").getValue();
			data.PhaseoutDate = oView.byId("proj_date").getValue();
			data.EcnNr = oView.byId("ecn_number").getValue();

			// call function getSwitchValue to set right values for the back-end
			data.InfoA = this.getSwitchValue(oView.byId("lessons_Learned_switch").getState());
			data.InfoB = this.getSwitchValue(oView.byId("proj_Team_Defined_switch").getState());
			data.InfoC = this.getSwitchValue(oView.byId("proj_Kickoff_Done").getState());

			var oModel = this.getView().getModel("project");
			var t = this;

			var _csrfToken = "";
			jQuery.ajax({
				url: "/sap/opu/odata/SAP/ZPLM_PROJINFO_SRV/",
				async: false,
				headers: {
					"X-CSRF-Token": "Fetch",
					"X-Requested-With": "XMLHttpRequest",
					"DataServiceVersion": "2.0"
				},
				type: "GET",
				contentType: "application/json",
				dataType: 'json',
				success: function(data, textStatus, jqXHR) {
					_csrfToken = jqXHR.getResponseHeader('x-csrf-token');
				}
			});

			var oURL = "/sap/opu/odata/SAP/ZPLM_PROJINFO_SRV/ItPhaseoutSet";
			$.ajax({
				type: 'POST',
				url: oURL,
				headers: {
					"X-CSRF-Token": _csrfToken,
					"Accept": 'application/json',
					"Content-Type": 'application/json'
				},
				cache: false,
				processData: false,
				data: JSON.stringify(data),
				dataType: 'json',
				success: function(success) {
					sap.m.MessageToast.show("Projekt erfolgreich angelegt.");
					t.uploadCSV();
				},
				error: function(err) {
					console.log(err);
				}
			});
		},
		fileChange: function(oEvent) {
			this.file = oEvent.getParameters().files[0];
		},
		uploadCSV: function() {
			var _csrfToken = "";
			var oFileUploader = this.getView().byId("idfileUploader");
			var t = this;

			oFileUploader.addHeaderParameter(new sap.ui.unified.FileUploaderParameter({
				value: _csrfToken
			}));

			jQuery.ajax({
				url: "/sap/opu/odata/SAP/ZPLM_CSV_SRV_02/",
				async: false,
				headers: {
					"X-CSRF-Token": "Fetch",
					"X-Requested-With": "XMLHttpRequest",
					"DataServiceVersion": "2.0"
				},
				type: "GET",
				contentType: "application/json",
				dataType: 'json',
				success: function(data, textStatus, jqXHR) {
					_csrfToken = jqXHR.getResponseHeader('x-csrf-token');
				}
			});
			var oURL = "/sap/opu/odata/SAP/ZPLM_CSV_SRV_02/CtCsvupSet('" + oFileUploader.getValue() + "')/$value";
			$.ajax({
				type: 'PUT',
				url: oURL,
				headers: {
					"X-CSRF-Token": _csrfToken,
					"X-Requested-With": "XMLHttpRequest",
					"DataServiceVersion": "2.0"
				},
				cache: false,
				contentType: ["csv"],
				processData: false,
				data: this.file,
				success: function(success) {
					sap.m.MessageToast.show("File upload successful.");
					console.log("File upload successful.");
					this.file = null;
					t.onCancel();
					t.navToHome();
				},
				error: function(err) {
					sap.m.MessageToast.show("Error: " + err);
				}
			});
		},
		navToHome: function() {
			this.oRouter.navTo("overview");
		},
		getSwitchValue: function(switchValue) {
			if (switchValue == true) {
				return "x";
			} else if (switchValue == false) {
				return "";
			}
		}

	});

});