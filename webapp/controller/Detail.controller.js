sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("PLM.controller.Detail", {

	onInit: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("detail").attachPatternMatched(this._onObjectMatched, this);
		},
		_onObjectMatched: function(oEvent) {
			this.pId = oEvent.getParameter("arguments").sId;
			this.Matnr = oEvent.getParameter("arguments").Matnr;
			var t = this;
			var oModel = this.getView().getModel("main");
			oModel.read("/ET_MATSTATSet(I_PROJECT_ID='" + this.pId + "',Matnr='" + this.Matnr + "')", {
				success: function(oData) {
					t.getView().byId("page").setTitle("Matnr: " + t.Matnr);
					t.getView().byId("Maktx").setTitle(oData.Maktx);
					t.getView().byId("Werks").setText(oData.Werks);
					t.getView().byId("Mstae").setText(oData.Mstae);
					t.getView().byId("Mmsta").setText(oData.Mmsta);
					t.getView().byId("Mstde").setText(oData.Mstde);
					t.getView().byId("Ean11").setText(oData.Ean11);
					t.getView().byId("EKWSL").setText(oData.Ekwsl);
					t.getView().byId("FORMT").setText(oData.Formt);
					t.getView().byId("MATKL").setText(oData.Matkl);
					t.getView().byId("MSTAV").setText(oData.Mstav);
					t.getView().byId("MSTDV").setText(oData.Mstdv);
					t.getView().byId("PRDHA").setText(oData.Prdha);
					t.getView().byId("SPART").setText(oData.Spart);
					t.getView().byId("BESKZ").setText(oData.Beskz);
					t.getView().byId("BSTFE").setText(oData.Bstfe);
					t.getView().byId("BSTMA").setText(oData.Bstma);
					t.getView().byId("BSTMI").setText(oData.Bstmi);
					t.getView().byId("DISLS").setText(oData.Disls);
					t.getView().byId("DISMM").setText(oData.Dismm);
					t.getView().byId("DISPO").setText(oData.Dispo);
					t.getView().byId("EISLO").setText(oData.Eislo);
					t.getView().byId("EKGRP").setText(oData.Ekgrp);
					t.getView().byId("MMSTD").setText(oData.Mmstd);
					t.getView().byId("PRCTR").setText(oData.Prctr);
					t.getView().byId("RGEKZ").setText(oData.Rgekz);
					t.getView().byId("SOBSK").setText(oData.Sobsk);
					t.getView().byId("SOBSL").setText(oData.Sobsl);
					t.getView().byId("PRDHA").setText(oData.Prdha);
					t.getView().byId("SPRAS").setText(oData.Spras);
				}
			});
		},
		onNavBack: function() {
					var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
					oRouter.navTo("worklist", {
						sId: this.pId
					});
				}

	});

});