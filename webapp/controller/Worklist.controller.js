sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter", "sap/ui/model/FilterOperator",
	"sap/m/MessageToast", "sap/m/MessageBox"
], function(Controller, JSONModel, Filter,
	FilterOperator, MessageToast, MessageBox) {
	"use strict";

	return Controller.extend("PLM.controller.Worklist", {

		onInit: function() {

			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);

			window.oRoute = this.oRouter;
			this.oRouter.getRoute("worklist").attachPatternMatched(this._onObjectMatched, this);

			this._mFilters = {
				"status50": [new sap.ui.model.Filter(
					"Mstae", "EQ", 50)],
				"status55": [new sap.ui.model.Filter(
					"Mstae", "EQ", 55)],
				"status60": [new sap.ui.model.Filter(
					"Mstae", "EQ", 60)],
				"status65": [new sap.ui.model.Filter(
					"Mstae", "EQ", 65)],
				"status70": [new sap.ui.model.Filter(
					"Mstae", "EQ", 70)],
				"status75": [new sap.ui.model.Filter(
					"Mstae", "EQ", 75)],
				"status80": [new sap.ui.model.Filter(
					"Mstae", "EQ", 80)],
				"status85": [new sap.ui.model.Filter(
					"Mstae", "EQ", 85)],
				"status90": [new sap.ui.model.Filter(
					"Mstae", "EQ", 90)],
				"all": []
			};
			this._oTable = this.getView().byId("table");
		},

		_onObjectMatched: function(oEvent) {
			var oTable = this.byId("table");
			var projectId = oEvent.getParameter("arguments").sId;
			this.pId = oEvent.getParameter("arguments").sId;
			window.pId = this.pId;
			//column list item creation
			var oTemplate = new sap.m.ColumnListItem({
				type: "Navigation",
				press: this.onPress,
				cells: [new sap.m.ObjectIdentifier({
						title: "{Matnr}"
					}),
					new sap.m.ObjectIdentifier({
						title: "{Maktx}"
					}),
					new sap.m.ObjectIdentifier({
						title: "{Mstae}"
					}),
					new sap.m.ObjectIdentifier({
						title: "{Werks}"
					}),
					new sap.m.ObjectIdentifier({
						title: "{Mmsta}"
					}),
					new sap.m.ObjectIdentifier({
						title: "{Mstde}"
					})
				]
			});

			var sServiceUrl = "/sap/opu/odata/SAP/ZPLM_SRV";
			var oModel = new sap.ui.model.odata.ODataModel(sServiceUrl, true);
			oTable.setModel(oModel);

			//Setting model to the table
			oTable.bindAggregation("items", {
				path: "/ET_MATSTATSet",
				template: oTemplate,
				filters: [new sap.ui.model.Filter("I_PROJECT_ID", sap.ui.model.FilterOperator.EQ, projectId)]
			});
			this.updateStatus();
		},

		phaseOutPress: function() {
			var oTable = this.byId("table");
			var con = oTable.getSelectedContexts();

			var items = con.map(function(c) {
				return c.getObject();
			});

			this.data = [];
			var _csrfToken = "";
			jQuery.ajax({
				url: "/sap/opu/odata/SAP/ZPLM_STATUS_SRV/",
				async: false,
				headers: {
					"X-CSRF-Token": "Fetch",
					"X-Requested-With": "XMLHttpRequest",
					"DataServiceVersion": "2.0"
				},
				type: "GET",
				contentType: "application/json",
				dataType: 'json',
				success: function(data, textStatus, jqXHR) {
					_csrfToken = jqXHR.getResponseHeader('x-csrf-token');
				}
			});

			for (var i = 0; i < items.length; i++) {
				var data = {
					"Matnr": items[i].Matnr,
					"Mstae": items[i].Mstae
				};
				$.ajax({
					type: 'POST',
					url: "/sap/opu/odata/SAP/ZPLM_STATUS_SRV/LtMatnrSet",
					headers: {
						"X-CSRF-Token": _csrfToken,
						"Accept": 'application/json',
						"Content-Type": 'application/json'
					},
					cache: false,
					processData: false,
					data: JSON.stringify(data),
					success: function(succ) {
						sap.m.MessageToast.show("Status successful updated.");
					},
					error: function(err) {
						console.log(err);
					}
				});
			}
			this.onRefresh();
			this.updateStatus();
		},
		/* =========================================================== */
		/* event handlers */
		/* =========================================================== */

		/**
		 * Triggered by the table's
		 * 'updateFinished' event: after new
		 * table data is available, this handler
		 * method updates the table counter.
		 * This should only happen if the update
		 * was successful, which is why this
		 * handler is attached to
		 * 'updateFinished' and not to the
		 * table's list binding's 'dataReceived'
		 * method.
		 * 
		 * @param {sap.ui.base.Event}
		 *            oEvent the update finished
		 *            event
		 * @public
		 */
		onUpdateFinished: function(oEvent) {

		},

		updateStatus: function() {
			var t = this;
			var oModel2 = this.getView().getModel("main");
			//var oModel2 = new sap.ui.model.odata.v2.ODataModel("sap/opu/odata/ZPLM_SRV");
			oModel2.read("/ET_MATSTATSet/$count/", {
				filters: [new sap.ui.model.Filter("I_PROJECT_ID", sap.ui.model.FilterOperator.EQ, this.pId)],
				success: function(oData) {
					t.getView().byId("all").setCount(oData);
				}
			});

			oModel2.read("/ET_MATSTATSet/$count/", {
				filters: [new sap.ui.model.Filter("I_PROJECT_ID", sap.ui.model.FilterOperator.EQ, this.pId),
					new sap.ui.model.Filter("Mstae", sap.ui.model.FilterOperator.EQ, "55")
				],
				success: function(data) {
					t.getView().byId("status55").setCount(data);
				}
			});

			oModel2.read("/ET_MATSTATSet/$count/", {
				filters: [new sap.ui.model.Filter("I_PROJECT_ID", sap.ui.model.FilterOperator.EQ, this.pId),
					new sap.ui.model.Filter("Mstae", sap.ui.model.FilterOperator.EQ, "60")
				],
				success: function(data) {
					t.getView().byId("status60").setCount(data);
				}
			});

			oModel2.read("/ET_MATSTATSet/$count/", {
				filters: [new sap.ui.model.Filter("I_PROJECT_ID", sap.ui.model.FilterOperator.EQ, this.pId),
					new sap.ui.model.Filter("Mstae", sap.ui.model.FilterOperator.EQ, "65")
				],
				success: function(data) {
					t.getView().byId("status65").setCount(data);
				}
			});

			oModel2.read("/ET_MATSTATSet/$count/", {
				filters: [new sap.ui.model.Filter("I_PROJECT_ID", sap.ui.model.FilterOperator.EQ, this.pId),
					new sap.ui.model.Filter("Mstae", sap.ui.model.FilterOperator.EQ, "70")
				],
				success: function(data) {
					t.getView().byId("status70").setCount(data);
				}
			});

			oModel2.read("/ET_MATSTATSet/$count/", {
				filters: [new sap.ui.model.Filter("I_PROJECT_ID", sap.ui.model.FilterOperator.EQ, this.pId),
					new sap.ui.model.Filter("Mstae", sap.ui.model.FilterOperator.EQ, "75")
				],
				success: function(data) {
					t.getView().byId("status75").setCount(data);
				}
			});

			oModel2.read("/ET_MATSTATSet/$count/", {
				filters: [new sap.ui.model.Filter("I_PROJECT_ID", sap.ui.model.FilterOperator.EQ, this.pId),
					new sap.ui.model.Filter("Mstae", sap.ui.model.FilterOperator.EQ, "80")
				],
				success: function(data) {
					t.getView().byId("status80").setCount(data);
				}
			});

			oModel2.read("/ET_MATSTATSet/$count/", {
				filters: [new sap.ui.model.Filter("I_PROJECT_ID", sap.ui.model.FilterOperator.EQ, this.pId),
					new sap.ui.model.Filter("Mstae", sap.ui.model.FilterOperator.EQ, "85")
				],
				success: function(data) {
					t.getView().byId("status85").setCount(data);
				}
			});

			oModel2.read("/ET_MATSTATSet/$count/", {
				filters: [new sap.ui.model.Filter("I_PROJECT_ID", sap.ui.model.FilterOperator.EQ, this.pId),
					new sap.ui.model.Filter("Mstae", sap.ui.model.FilterOperator.EQ, "90")
				],
				success: function(data) {
					t.getView().byId("status90").setCount(data);
				}
			});
		},

		/**
		 * Event handler when a table item gets pressed
		 */
		onPress: function(oEvent) {
			// The source is the list item that got pressed
			var sPath = oEvent.getSource().oBindingContexts.undefined.sPath;
			var result = sPath.match(/\d+/)[0];

			window.oRoute.navTo("detail", {
				Matnr: result,
				sId: window.pId
			});

		},

		/**
		 * Navigates back in the browser
		 * history, if the entry was created by
		 * this app. If not, it navigates to the
		 * Fiori Launchpad home page.
		 * 
		 * @public
		 */
		onNavBack: function() {
			this.oRouter.navTo("projects");
			/*
			var oHistory = sap.ui.core.routing.History.getInstance(),
				sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				// The history contains a previous entry
				history.go(-1);
			}
			*/
		},

		onSearch: function(oEvent) {
			// add filter for search
			var aFilters = [];
			var sQuery = oEvent.getSource().getValue();
			if (sQuery && sQuery.length > 0) {
				var filter_Mstae = new Filter("Mstae", sap.ui.model.FilterOperator.EQ, sQuery);
				aFilters.push(filter_Mstae);
			}

			// update list binding
			var oTable = this.byId("table");
			var oBinding = oTable.getBinding("items");
			oBinding.filter(aFilters);
		},
		onSuggest: function(oEvent) {

		},
		/**
		 * Event handler for refresh event.
		 * Keeps filter, sort and group settings
		 * and refreshes the list binding.
		 * 
		 * @public
		 */
		onRefresh: function() {
			var oTable = this.byId("table");
			oTable.getBinding("items").refresh();
		},

		/* =========================================================== */
		/* internal methods */
		/* =========================================================== */
		/*
		 * Shows the selected item on the object
		 * page On phones a additional history
		 * entry is created
		 * 
		 * @param {sap.m.ObjectListItem}
		 *            oItem selected Item
		 * @private
		 */
		_showObject: function(oItem) {
			this.getRouter().navTo("object", {
				objectId: oItem.getBindingContext().getProperty("ProductID")
			});
		},

		/**
		 * Internal helper method to apply both
		 * filter and search state together on
		 * the list binding
		 * 
		 * @param {array}
		 *            oTableSearchState an array
		 *            of filters for the search
		 * @private
		 */
		_applySearch: function(
			oTableSearchState) {
			var oModel = this.getModel("worklistView");
			this._oTable.getBinding("items").filter(oTableSearchState, "Application");
			// changes the noDataText of the
			// list in case there are no filter
			// results
			if (oTableSearchState.length !== 0) {
				oModel.setProperty("/tableNoDataText", this.getResourceBundle().getText("worklistNoDataWithSearchText"));
			}
		},
		/**
		 * Displays an error message dialog. The
		 * displayed dialog is content density
		 * aware.
		 * 
		 * @param {string}
		 *            sMsg The error message to
		 *            be displayed
		 * @private
		 */
		_showErrorMessage: function(sMsg) {
			MessageBox.error(sMsg, {
				styleClass: this.getOwnerComponent().getContentDensityClass()
			});
		},

		/**
		 * Event handler when a filter tab gets
		 * pressed
		 * 
		 * @param {sap.ui.base.Event}
		 *            oEvent the filter tab
		 *            event
		 * @public
		 */
		onQuickFilter: function(oEvent) {
			var oTable = this.byId("table");
			var oBinding = oTable.getBinding("items"),
				sKey = oEvent.getParameter("selectedKey");
			oBinding.filter(this._mFilters[sKey]);
		},
		/**
		 * Error and success handler for the
		 * unlist action.
		 * 
		 * @param {string}
		 *            sProductId the product ID
		 *            for which this handler is
		 *            called
		 * @param {boolean}
		 *            bSuccess true in case of a
		 *            success handler, else
		 *            false (for error handler)
		 * @param {number}
		 *            iRequestNumber the counter
		 *            which specifies the
		 *            position of this request
		 * @param {number}
		 *            iTotalRequests the number
		 *            of all requests sent
		 * @private
		 */
		_handleUnlistActionResult: function(
			sProductId, bSuccess,
			iRequestNumber, iTotalRequests) {
			// we could create a counter for
			// successful and one for failed
			// requests
			// however, we just assume that
			// every single request was
			// successful and display a success
			// message once
			if (iRequestNumber === iTotalRequests) {
				MessageToast
					.show(this
						.getModel(
							"i18n")
						.getResourceBundle()
						.getText(
							"StockRemovedSuccessMsg", [iTotalRequests]));
			}
		},

		/**
		 * Error and success handler for the
		 * reorder action.
		 * 
		 * @param {string}
		 *            sProductId the product ID
		 *            for which this handler is
		 *            called
		 * @param {boolean}
		 *            bSuccess true in case of a
		 *            success handler, else
		 *            false (for error handler)
		 * @param {number}
		 *            iRequestNumber the counter
		 *            which specifies the
		 *            position of this request
		 * @param {number}
		 *            iTotalRequests the number
		 *            of all requests sent
		 * @private
		 */
		_handleReorderActionResult: function(
			sProductId, bSuccess,
			iRequestNumber, iTotalRequests) {
			// we could create a counter for
			// successful and one for failed
			// requests
			// however, we just assume that
			// every single request was
			// successful and display a success
			// message once
			if (iRequestNumber === iTotalRequests) {
				MessageToast
					.show(this
						.getModel(
							"i18n")
						.getResourceBundle()
						.getText(
							"StockUpdatedSuccessMsg", [iTotalRequests]));
			}
		},

		/**
		 * Event handler for the unlist button.
		 * Will delete the product from the
		 * (local) model.
		 * 
		 * @public
		 */
		onUnlistObjects: function() {
			var aSelectedProducts, i, sPath, oProduct, oProductId;

			aSelectedProducts = this.byId("table").getSelectedItems();
			if (aSelectedProducts.length) {
				for (i = 0; i < aSelectedProducts.length; i++) {
					oProduct = aSelectedProducts[i];
					oProductId = oProduct.getBindingContext().getProperty("ProductID");
					sPath = oProduct.getBindingContextPath();
					this.getModel().remove(
						sPath, {
							success: this._handleUnlistActionResult.bind(this, oProductId, true, i + 1, aSelectedProducts.length),
							error: this._handleUnlistActionResult.bind(this, oProductId, false, i + 1, aSelectedProducts.length)
						});
				}
			} else {
				this._showErrorMessage(this.getModel("i18n").getResourceBundle().getText("TableSelectProduct"));
			}
		},

		/**
		 * Event handler for the reorder button.
		 * Will reorder the product by updating
		 * the (local) model
		 * 
		 * @public
		 */
		onUpdateStockObjects: function() {
			var aSelectedProducts, i, sPath, oProductObject;

			aSelectedProducts = this.byId("table").getSelectedItems();
			if (aSelectedProducts.length) {
				for (i = 0; i < aSelectedProducts.length; i++) {
					sPath = aSelectedProducts[i].getBindingContextPath();
					oProductObject = aSelectedProducts[i].getBindingContext().getObject();
					oProductObject.UnitsInStock += 10;
					this.getModel().update(sPath, oProductObject, {
						success: this._handleReorderActionResult.bind(this, oProductObject.ProductID, true, i + 1, aSelectedProducts.length),
						error: this._handleReorderActionResult.bind(this, oProductObject.ProductID, false, i + 1, aSelectedProducts.length)
					});
				}
			} else {
				this._showErrorMessage(this.getModel("i18n").getResourceBundle().getText("TableSelectProduct"));
			}
		}
	});

});